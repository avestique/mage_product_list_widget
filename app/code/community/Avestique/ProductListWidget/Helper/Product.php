<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Product.php
 */

class Avestique_ProductListWidget_Helper_Product
{
    public function isNew($product)
    {
        $from = $to = false;

        $current_date = new DateTime(Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));

        if ($from_date = $product->getData('news_from_date'))
            $from = $current_date >= new DateTime($from_date);

        if ($to_date = $product->getData('news_to_date'))
            $to = $current_date <= new DateTime($to_date);

        if ($from_date && $to_date && (new DateTime($from_date) > new DateTime($to_date)))
        {
            $from = $to = false;
        }

        return $from || $to ? true : false;
    }
}