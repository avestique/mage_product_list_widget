<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file ${FILE_NAME}
 */ 
class Avestique_ProductListWidget_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getMaxLevel($currentLevel = null)
    {
        $collection = Mage::getResourceModel('catalog/category_collection');

        $collection->getSelect()->reset(Zend_Db_Select::COLUMNS)->columns('MAX(level) as level');

        foreach($collection as $item)
        {
            $max = $item->level - 1;
        }

        return $max < $currentLevel || $currentLevel === null ? $max : $currentLevel;
    }
}