<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Select.php
 */
class Avestique_ProductListWidget_Block_Catalog_Chooser extends Mage_Adminhtml_Block_Catalog_Category_Widget_Chooser
{
    /**
     * Block construction
     * Defines tree template and init tree params
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('avestique/catalog/category/widget/tree.phtml');
        $this->_withProductCount = false;

        if (!$this->getSelectedCategories() && $this->getRequest()->getPost('element_value'))
        {
            $this->setSelectedCategories( explode(',', $this->getRequest()->getPost('element_value')) );
        }
    }

    public function getRootNode()
    {
        $root = $this->getRoot();
        if ($root && in_array($root->getId(), $this->getSelectedCategories())) {
            $root->setChecked(true);
        }
        return $root;
    }

    public function getRoot($parentNodeCategory=null, $recursionLevel=3)
    {
        if (!is_null($parentNodeCategory) && $parentNodeCategory->getId()) {
            return $this->getNode($parentNodeCategory, $recursionLevel);
        }

        $root = Mage::registry('root');

        if (is_null($root))
        {
            $storeId = (int) $this->getRequest()->getParam('store');

            if ($storeId) {
                $store = Mage::app()->getStore($storeId);
                $rootId = $store->getRootCategoryId();
            }
            else {
                $rootId = Mage_Catalog_Model_Category::TREE_ROOT_ID;
            }

            $ids = $this->getSelectedCategoriesPathIds($rootId);
            $tree = Mage::getResourceSingleton('catalog/category_tree')
                ->loadByIds($ids, false, false);

            //if ($this->getCategory()) {
            //    $tree->loadEnsuredNodes($this->getCategory(), $tree->getNodeById($rootId));
            //}

            $tree->addCollectionData($this->getCategoryCollection());

            $root = $tree->getNodeById($rootId);

            if ($root && $rootId != Mage_Catalog_Model_Category::TREE_ROOT_ID) {
                $root->setIsVisible(true);
                if ($this->isReadonly()) {
                    $root->setDisabled(true);
                }
            }
            elseif($root && $root->getId() == Mage_Catalog_Model_Category::TREE_ROOT_ID) {
                $root->setName(Mage::helper('catalog')->__('Root'));
            }

            Mage::register('root', $root);
        }

        return $root;
    }

    /**
     * Get JSON of a tree node or an associative array
     *
     * @param Varien_Data_Tree_Node|array $node
     * @param int $level
     * @return string
     */
    protected function _getNodeJson($node, $level = 0)
    {
        // create a node from data array
        if (is_array($node)) {
            $node = new Varien_Data_Tree_Node($node, 'entity_id', new Varien_Data_Tree);
        }

        $item = array();
        $item['text'] = $this->buildNodeName($node);

        //$rootForStores = Mage::getModel('core/store')->getCollection()->loadByCategoryIds(array($node->getEntityId()));
        $rootForStores = in_array($node->getEntityId(), $this->getRootIds());

        $item['id']  = $node->getId();
        $item['store']  = (int) $this->getStore()->getId();
        $item['path'] = $node->getData('path');

        if (in_array($node->getId(), $this->getSelectedCategories()))
            $item['checked'] = true;

        $item['cls'] = 'folder ' . ($node->getIsActive() ? 'active-category' : 'no-active-category');
        //$item['allowDrop'] = ($level<3) ? true : false;
        $allowMove = $this->_isCategoryMoveable($node);
        $item['allowDrop'] = $allowMove;
        // disallow drag if it's first level and category is root of a store
        $item['allowDrag'] = $allowMove && (($node->getLevel()==1 && $rootForStores) ? false : true);

        if ((int)$node->getChildrenCount()>0) {
            $item['children'] = array();
        }

        $isParent = $this->_isParentSelectedCategory($node);

        if ($node->hasChildren()) {
            $item['children'] = array();
            if (!($this->getUseAjax() && $node->getLevel() > 1 && !$isParent)) {
                foreach ($node->getChildren() as $child) {
                    $item['children'][] = $this->_getNodeJson($child, $level+1);
                }
            }
        }

        if ($isParent || $node->getLevel() < 2) {
            $item['expanded'] = true;
        }

        return $item;
    }

    /**
     * Prepare chooser element HTML
     *
     * @param Varien_Data_Form_Element_Abstract $element Form Element
     * @return Varien_Data_Form_Element_Abstract
     */
    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $uniqId = Mage::helper('core')->uniqHash($element->getId());
        $sourceUrl = $this->getUrl('av_plw/adminhtml_catalog_category_widget/chooser', array('uniq_id' => $uniqId, 'use_massaction' => true));

        $chooser = $this->getLayout()->createBlock('widget/adminhtml_widget_chooser')
            ->setElement($element)
            ->setTranslationHelper($this->getTranslationHelper())
            ->setConfig($this->getConfig())
            ->setFieldsetId($this->getFieldsetId())
            ->setSourceUrl($sourceUrl)
            ->setUniqId($uniqId);

        if ($element->getValue()) {
            $value = explode(',', $element->getValue());

            $collection = Mage::getModel('catalog/category')->getCollection()
                                ->addAttributeToSelect('name', true)
                                ->addFieldToFilter('entity_id', array('in' => $value));

            $labels = array();

            foreach($collection as $category)
            {
                $labels[] = $category->getName();
            }

            $chooser->setLabel(implode(', ', $labels));
        }

        $element->setData('after_element_html', $chooser->toHtml());
        return $element;
    }


    /**
     * Tree JSON source URL
     *
     * @todo $ids - it has list of category id. It doesn't do anything after
     * @return string
     */
    public function getLoadTreeUrl($ids = null)
    {
        return $this->getUrl('av_plw/adminhtml_catalog_category_widget/categoriesJson', array(
            '_current'=>true,
            'uniq_id' => $this->getId(),
            'use_massaction' => $this->getUseMassaction(),
            'ids' => $ids
        ));
    }

    /**
     * Prepare html output
     *
     * @return string
     */
    protected function getButtonOK()
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
            'label'     => Mage::helper('core')->__('Save'),
            'onclick'   => "return okButton.onPress()",
            'class'     => "scalable",
            'type'      => 'button',
            //'style'     => 'margin:20px 0 10px 20px;',
            'id'        => "",
        ))->toHtml();
    }
}