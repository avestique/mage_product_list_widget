<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file List.php
 */

class Avestique_ProductListWidget_Block_Catalog
    extends Mage_Catalog_Block_Product_List
        implements Mage_Widget_Block_Interface
{
    const PRODUCT_LIMIT = 100;

    public function _construct()
    {
        parent::_construct();

        /*
         * ["selected_category"]=>
           ["depth_to_select"]=>
           ["limit"]=>
           ["view_type"]=>
           ['column_count']
         */

        $this->setTemplate('avestique/catalog/product/list.phtml');
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->getViewType()==1 ? 'list' : 'grid';
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection()
    {
        if (is_null($this->_productCollection)) {

            $rootCategoryID = Mage::app()->getStore()->getRootCategoryId();

            $categoryCollection = Mage::getModel('av_plw/category')->getCategoryCollection($this->getSelectedCategory(), 2);

            $categoriesID = array_keys($categoryCollection);

            $this->_productCollection = Mage::getModel('av_plw/product')->getProductCollection($categoriesID, $this->getLimit());
        }

        return $this->_productCollection;
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        Mage::dispatchEvent('avestique_block_product_list_widget_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();

        return $this;
    }

    public function isNew($product)
    {
        return Mage::helper('av_plw/product')->isNew($product);
    }
}