<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Stores.php
 */

class Avestique_ProductListWidget_Model_Source_Stores
{
    public function toOptionArray()
    {
        $list[] = array('label' => ' ---- ', 'value' => '');

        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {

                $stores = $group->getStores();

                foreach ($stores as $store) {
                    $list[] = array('value' => $store->getId(),
                                    'label' => $website->getName() . ' : ' . $group->getName() . ' : ' . $store->getName()
                    );
                }
            }
        }

        return $list;
    }
}