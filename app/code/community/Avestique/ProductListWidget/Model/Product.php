<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Product.php
 */

class Avestique_ProductListWidget_Model_Product extends Mage_Core_Model_Abstract
{
    public function getProductCollection($categoriesID, $limit = NULL)
    {
        $_productCollection = Mage::getModel('catalog/product')->getCollection()
            ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addAttributeToFilter('category_id', array('in' => $categoriesID) )
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite()
            ->setOrder('name', Varien_Data_Collection::SORT_ORDER_ASC);

        //->addAttributeToSort('created_at', 'desc');

        $select = $_productCollection->getSelect();

        $select->group('product_id')->limit($limit ? $limit : self::PRODUCT_LIMIT);

        $_productCollection->getSelect($select);

        return $_productCollection;
    }
}