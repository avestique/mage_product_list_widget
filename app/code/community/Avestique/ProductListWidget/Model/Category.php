<?php
/**
 * Avestique Developer
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to info@avestique.ru so we can send you a copy immediately.
 *
 * @category    Avestique
 * @package     Avestique_Core
 * @copyright   Copyright (c) 2013 Avestique Developer. (http://www.avestique.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *
 * @file Category.php
 */

class Avestique_ProductListWidget_Model_Category extends Mage_Catalog_Model_Resource_Abstract
{
    public function getCategoryCollection($selectedCategory, $level = NULL)
    {
        $storeId = Mage::app()->getStore()->getId();

        //$expr = new Zend_Db_Expr('`level` + ' . ($level ? $level : Mage::helper('av_plw')->getMaxLevel() ));
        $categoryCollection = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect(array('name', 'level', 'path'))
            //->addFieldToFilter('level', array('lteq' => $expr))
            //->addFieldToFilter('entity_id', explode(',', $this->getSelectedCategory()))
            ->addUrlRewriteToResult()
            ->setStoreId(Mage::app()->getStore()->getId())
            ->setOrder('level', Varien_Data_Collection::SORT_ORDER_ASC);


        $select = $categoryCollection->getSelect();

        $selectedCategory = explode(',', $selectedCategory);

        $OR = '';

        foreach($selectedCategory as $idCategory)
        {
            $OR .= " OR ( FIND_IN_SET($idCategory, REPLACE(`path`, '/', ',')) ) ";
        }

        $select->where("entity_id IN (?) $OR ",  implode(',', $selectedCategory));

        $categoryCollection->getSelect($select);

        if ($level !== NULL)
        {
            $roots = array();

            foreach($categoryCollection as $item)
            {
                if (in_array($item->getId(), $selectedCategory))
                {
                    $roots[$item->getId()] = $item->getLevel();
                }
            }

            $remove = array();
            foreach($categoryCollection as $id => $item)
            {
                if (!in_array($item->getId(), $selectedCategory))
                {
                    $path = explode('/', $item->getPath());

                    foreach($roots as $rootID => $nodeLevel)
                    {
                        if (in_array($rootID, $path) && $item->getLevel() > $nodeLevel + $level)
                        {
                            $remove[] = $id;
                        }
                    }
                }
            }
        }

        foreach($remove as $id)
            $categoryCollection->removeItemByKey($id);

        return $categoryCollection->getItems();
    }
}